//import vue router
import { createRouter, createWebHistory } from 'vue-router';

//import axios
import axios from 'axios';

//default base URL / EndPoint API
axios.defaults.baseURL = "http://localhost:8080";

//define a routes
const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import( /* webpackChunkName: "Home" */ "../views/home/Index.vue"),
    },
]

//create router
const router = createRouter({
    history: createWebHistory(),
    routes // <-- routes
})

export default router